// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Game Engine constants.h v1.5
// Last modification: Feb-17-2013

#ifndef _CONSTANTS_H            // Prevent multiple definitions if this 
#define _CONSTANTS_H            // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include <windows.h>

//=============================================================================
// Function templates for safely dealing with pointer referenced items.
// The functions defined by these templates may be called using a normal
// function call syntax. The compiler will create a function that replaces T
// with the type of the calling parameter.
//=============================================================================
// Safely release pointer referenced item
template <typename T>
inline void safeRelease(T& ptr)
{
    if (ptr)
    { 
        ptr->Release(); 
        ptr = NULL;
    }
}
#define SAFE_RELEASE safeRelease            // for backward compatiblility

// Safely delete pointer referenced item
template <typename T>
inline void safeDelete(T& ptr)
{
    if (ptr)
    { 
        delete ptr; 
        ptr = NULL;
    }
}
#define SAFE_DELETE safeDelete              // for backward compatiblility

// Safely delete pointer referenced array
template <typename T>
inline void safeDeleteArray(T& ptr)
{
    if (ptr)
    { 
        delete[] ptr; 
        ptr = NULL;
    }
}
#define SAFE_DELETE_ARRAY safeDeleteArray   // for backward compatiblility

// Safely call onLostDevice
template <typename T>
inline void safeOnLostDevice(T& ptr)
{
    if (ptr)
        ptr->onLostDevice(); 
}
#define SAFE_ON_LOST_DEVICE safeOnLostDevice    // for backward compatiblility

// Safely call onResetDevice
template <typename T>
inline void safeOnResetDevice(T& ptr)
{
    if (ptr)
        ptr->onResetDevice(); 
}
#define SAFE_ON_RESET_DEVICE safeOnResetDevice  // for backward compatiblility

//=============================================================================
//                  Constants
//=============================================================================

// window
const char CLASS_NAME[] = "BombermanMultiverse";
const char GAME_TITLE[] = "BombermanMultiverse";
const bool FULLSCREEN = false;              // windowed or fullscreen
const UINT GAME_WIDTH =  1600;               // width of game in pixels
const UINT GAME_HEIGHT = 900;               // height of game in pixels
 
// game
const double PI = 3.14159265;
const float FRAME_RATE = 480.0f;                // the target frame rate (frames/sec)
const float MIN_FRAME_RATE = 10.0f;             // the minimum frame rate
const float MIN_FRAME_TIME = 1.0f/FRAME_RATE;   // minimum desired time for 1 frame
const float MAX_FRAME_TIME = 1.0f/MIN_FRAME_RATE; // maximum time used in calculations
//collision
const RECT  COLLISION_RECTANGLE = { -16, -8, 16, 8 };
const RECT  COLLISION_BOX = { -14, -14, 14, 14 };

namespace Conor
{
	// graphic images
	const char MENU_IMAGE[] = "pictures\\Menus\\MainMenu.png";      // menu texture
	const char POINTER_IMAGE[] = "pictures\\Menus\\Pointer.png"; //meny texture
	const char START[] = "pictures\\Menus\\Start.png";      // menu texture
	const char CONTROLS[] = "pictures\\Menus\\Controls.png";      // menu texture
	const char QUIT[] = "pictures\\Menus\\Quit.png";      // menu texture
	const char BACK[] = "pictures\\Menus\\Back.png";      // menu texture
	const char CONTROLS_IMAGE[] = "pictures\\Menus\\ControlsMenu.png";      // menu texture
	const char MENUSPRITE_TEXTURES[] = "pictures\\Options.png";
	const char TILES[] = "pictures\\tiles.txt";     // game tiles
	const char BOMBERMAN_PLAYER1[] = "pictures\\Player\\BombermanPlayer1.png";
	const char BOMBERMAN_PLAYER2[] = "pictures\\Player\\BombermanPlayer2.png";
	// map data
	//const char MAP[] = "maps\\map1.txt";      // map data
	const char MAP[] = "maps\\map2.txt";
	//const char MAP[] = "maps\\map3.txt";
	//player data
	const char PLAYERS_IMAGE[] = "pictures\\Player\\PlayerTextures1.png";  // game textures
	const char PLAYER2_IMAGE[] = "pictures\\Player\\Player2Textures.png";
	//const char BOMB_IMAGE[] = "pictures\\Player\\Bomb.png";
	const char BOMB_IMAGE[] = "pictures\\bombTextures.png";
	const char DESBLOCK_IMAGE[] = "pictures\\Blocks\\crate.png";
	const char POWERUPBLOCK_IMAGE[] = "pictures\\power ups\\PowerUpTextures.png";
	const char EXPLOSION_IMAGE[] = "pictures\\Explode.png";
	const char MID_EXPLOSION_IMAGE[] = "pictures\\Explode2.png";
	const char LARGE_EXPLOSION_IMAGE[] = "pictures\\Explode3.png";
	// audio files required by audio.cpp
	// WAVE_BANK must be location of .xwb file.
	const char WAVE_BANK[] = "audio\\Win\\Wave Bank.xwb";
	// SOUND_BANK must be location of .xsb file.
	const char SOUND_BANK[] = "audio\\Win\\Sound Bank.xsb";

	// audio cues
	const char MUSIC1[] = "Music1";
	const char MENU[] = "Menu";
	const char DIED[] = "Died";
	const char CRYSTAL[] = "Crystal";
	const char PICKUP[] = "Pickup";
	const char EXPLODE[] = "Explode";
	const char WALK[] = "Walk";

}



// key mappings
// In this game simple constants are used for key mappings. If variables were used
// it would be possible to save and restore key mappings from a data file.
const UCHAR CONSOLE_KEY  = '`';         // ` key
const UCHAR ESC_KEY      = VK_ESCAPE;   // escape key
const UCHAR ALT_KEY      = VK_MENU;     // Alt key
const UCHAR ENTER_KEY    = VK_RETURN;   // Enter key

const UCHAR PLAYER1_LEFT_KEY = 'A';
const UCHAR PLAYER1_RIGHT_KEY = 'D';
const UCHAR PLAYER1_UP_KEY = 'W';
const UCHAR PLAYER1_DOWN_KEY = 'S';
const UCHAR PLAYER1_DROP_KEY = 'E';

const UCHAR PLAYER2_LEFT_KEY = VK_LEFT; // left arrow
const UCHAR PLAYER2_RIGHT_KEY = VK_RIGHT; // right arrow
const UCHAR PLAYER2_UP_KEY = VK_UP;   // up arrow
const UCHAR PLAYER2_DOWN_KEY = VK_DOWN; // down arrow
const UCHAR PLAYER2_DROP_KEY = VK_SPACE;


#endif
