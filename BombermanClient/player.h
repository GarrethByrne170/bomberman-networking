#ifndef PLAYER_H               // Prevent multiple definitions if this 
#define PLAYER_H              // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "constants.h"
#include "bomb.h"
#include "PowerUp.h"


namespace Conor
{
	const int WIDTH = 32;                   // image width
	const int HEIGHT = 32;                  // image height
	const int X = GAME_WIDTH / 2 - WIDTH / 2;   // location on screen
	const int Y = GAME_HEIGHT / 2 - HEIGHT / 2;
	const float ROTATION_RATE = (float)PI / 4; // radians per second
	const float SPEED = 10;                // 100 pixels per second
	const float MASS = 300.0f;              // mass
	const int   TEXTURE_COLS = 5;           // texture has 8 columns
	const int RADUIS =  WIDTH / 2;
	//Sprite Sheet
	const int   SPRITE_START_FRAME = 0;      // Player1 starts at frame 0
	const int   SPRITE_END_FRAME = 19;        // Player1 animation frames 0,1,2,3,4
	const float SPRITE_ANIMATION_DELAY = 0.2f;    // time between frames
	
	//Right
	const int   LEFT_START_FRAME = 0;      // Player1 starts at frame 0
	const int   LEFT_END_FRAME = 4;        // Player1 animation frames 0,1,2,3,4
	
	//Up
	const int   UP_START_FRAME = 5;      // Player1 starts at frame 5
	const int   UP_END_FRAME = 9;        // Player1 animation frames 5,6,7,8,9

	//Down
	const int   DOWN_START_FRAME = 10;      // Player1 starts at frame 5
	const int   DOWN_END_FRAME = 14;        // Player1 animation frames 5,6,7,8,9

	//Left
	const int   RIGHT_START_FRAME = 16;      // Player1 starts at frame 0
	const int   RIGHT_END_FRAME = 19;        // Player1 animation frames 0,1,2,3,4
	
	//Idle
	const int   IDLE_FRAME = 2;      // Player1 starts at frame 5
	//Bomb
	const int   BOMB_START_FRAME = 5;    // engine start frame
	const int   BOMB_END_FRAME = 9;      // engine end frame
	const float BOMB_ANIMATION_DELAY = 0.1f;  // time between frames

	//networking
	const int LEFT_BIT = 0x03;
	const int RIGHT_BIT = 0x04;
	const int FORWARD_BIT = 0x05;
	const int BACKWARDS_BIT = 0x06;
}
/*enum DIRECTION{
	NONE,
	NORTH,
	SOUTH,
	WEST,
	EAST
};*/
enum PLAYERSTATE{
	WALKING,
	STANDING
};
//struct for the player
struct PlayerStc{
	float X, Y;
	short score;
	float walkSpeed;
	UCHAR playerN;
	UCHAR buttons;
	//------flags-------
	//bit0 active
	//bit 1 isDead
	//bit 2 left bit
	//bit 3 right bit
	//bit 4 forward bit
	//bit 5 backwards bit
	UCHAR flags;
};
// inherits from Entity class
class Player : public Entity
{
private:
	Image   player;
	int const walkStep = 5;
	int playerID;
	int numberOfBombs;
	float walkSpeed;
	//DIRECTION dir;
	PLAYERSTATE state;
	float bombTimer;
	float bombPower;
	float oldX, oldY;
	int score;
	bool isArmour;
	bool isDead;


	// Network Variables
	char    netIP[16];      // IP address as dotted quad; nnn.nnn.nnn.nnn
	int     timeout;
	bool    connected;
	UCHAR   buttons;        // current key presses
	u_long  packetNumber;   // used to detect out of order packets
	UCHAR   playerN;        // our player number
	int     commWarnings;   // count of communication warnings
	int     commErrors;     // count of communication errors

public:
	// constructors
	Player();
	Player(int playerID);
	//destructor
	virtual ~Player(){}
	// inherited member functions
	virtual void draw();
	virtual bool initialize(Game *gamePtr, int width, int height, int ncols,
		TextureManager *textureM);
	void update(float frameTime);
	
		void handleInput(float frameTime);
		void placeBomb();

		//getters and setters
		int GetNumBombs(){ return numberOfBombs; }
		void setNumBombs(int b){ numberOfBombs = b; }
		float getWalkSpeed(){ return walkSpeed; }
		void setWalkSpeed(float s){ walkSpeed = s; }
		PLAYERSTATE getPlayerState(){ return state; }
		void setPlayerState(PLAYERSTATE s){ state = s; }
		float getBombTimer(){ return bombTimer; }
		void setBomberTimer(float t){ bombTimer = t; }
		float getBombPower(){ return bombPower; }
		void setBombPower(float p){ bombPower = p; }
		int getWalkStep(){ return walkStep; }
		void toOldPosition(){
			spriteData.x = oldX;
			spriteData.y = oldY;
		}
		void addPowerUP(PowerUp &p);
		int getScore(){ return score; }
		void setScore(int p){ score = p; }
		bool getIsArmoured(){ return isArmour; }
		void setIsArmoured(bool p){ isArmour = p; }
		void resetStats();
		bool getIsDead(){ return isDead; }
		void setIsDead(bool p){ isDead = p; }
		int getID(){ return playerID; }
		void setID(int p){ playerID = p; }

	//networking functions
		// Return IP address of this player as dotted quad; nnn.nnn.nnn.nnn
		const char* getNetIP()  { return netIP; }

		// Return netTimeout count
		int getTimeout()    { return timeout; }

		// Return netConnected boolean
		bool getConnected() { return connected; }

		// Return buttons
		UCHAR getButtons()      { return buttons; }

		// Return packetNumber
		u_long getPacketNumber() { return packetNumber; }

		// Return Ship Data
		PlayerStc getNetData();

		// Return player number
		int getPlayerN()        { return playerN; }

		// Return commWarnings
		int getCommWarnings()   { return commWarnings; }

		// Return commErrors
		int getCommErrors()     { return commWarnings; }

		// Sets all ship data from ShipStc sent from server to client
		void setNetData(PlayerStc ss);


		// Set IP address of this player as dotted quad; nnn.nnn.nnn.nnn
		void setNetIP(const char* address)  { strcpy_s(netIP, address); }

		// Set netConnected boolean
		void setConnected(bool c)    { connected = c; }

		// Set buttons
		void setButtons(UCHAR b)        { buttons = b; }

		// Set packetNumber
		void setPacketNumber(u_long n)  { packetNumber = n; }

		// Set commWarnings
		void setCommWarnings(int w)     { commWarnings = w; }

		// Set commErrors
		void setCommErrors(int e)       { commErrors = e; }

		// Set timeout
		void setTimeout(int t)          { timeout = t; }

		// Increment commWarnings
		void incCommWarnings()          { commWarnings++; }

		// Increment commErrors
		void incCommErrors()            { commErrors++; }

		// Increment netTimeout count
		void incTimeout()               { timeout++; }
};
#endif
