// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// dashboard.cpp v1.0

#include "Menu.h"
#include "audio.h"

//=============================================================================
// Constructor
//=============================================================================
MenuSprite::MenuSprite()
{
	switchOn = false;
	mouseClick = true;
	momentary = true;
}

//=============================================================================
// Initialize the Pushbutton
// Pre: *graphics = pointer to Graphics object
//      *textureM = pointer to TextureManager object
//      *in = pointer to Input object
//      hwnd = handle to window
//      left, top = screen location
//      scale = scaling (zoom) amount
//      type = true for momentary, false for toggle
// Post: returns true on success, false on error
//=============================================================================
bool MenuSprite::initialize(Graphics *graphics, TextureManager *textureM, Input *in, HWND h,
	int left, int top, float scale, bool type)
{

		try {
		Image::initialize(graphics, ConorMenu::IMAGE_WIDTH , ConorMenu::IMAGE_HEIGHT ,
			ConorMenu::TEXTURE_COLS, textureM);
		setCurrentFrame(ConorMenu::BUTTON_UP_FRAME);
		spriteData.x = (float)left;
		spriteData.y = (float)top;
		spriteData.scale = scale;
		hwnd = h;                       // handle to the window
		input = in;                     // the input object
		switchRect.left = left;
		switchRect.top = top;
		switchRect.right = (long)(left + spriteData.width * spriteData.scale);
		switchRect.bottom = (long)(top + spriteData.height * spriteData.scale);
		momentary = type;
	}
	catch (...)
	{
		return false;
	}
	//return okay
	return true;
}

//=============================================================================
// Checks for mouse click on pushbutton
//=============================================================================
void MenuSprite::update(float frameTime)
{
	if (!initialized || !visible)
		return;

	// calculate screen ratios incase window was resized
	RECT clientRect;
	GetClientRect(hwnd, &clientRect);
	float screenRatioX = (float)GAME_WIDTH / clientRect.right;
	float screenRatioY = (float)GAME_HEIGHT / clientRect.bottom;

	if (input->getMouseLButton())           // if mouse left button
	{
		// if mouse clicked inside switch
		if (input->getMouseX()*screenRatioX >= switchRect.left &&
			input->getMouseX()*screenRatioX <= switchRect.right &&
			input->getMouseY()*screenRatioY >= switchRect.top &&
			input->getMouseY()*screenRatioY <= switchRect.bottom)
		{

			if (mouseClick)
			{
				mouseClick = false;
				if (momentary)               // if momentary switch
					switchOn = true;
				else
					switchOn = !switchOn;   // toggle the switch
				if (switchOn)
				{
					setCurrentFrame(ConorMenu::BUTTON_DOWN_FRAME);
				}

				else
				{
					setCurrentFrame(ConorMenu::BUTTON_UP_FRAME);
				}
			}
		}
	}
	else
	{
		mouseClick = true;
		if (momentary)
		{
			switchOn = false;
			setCurrentFrame(ConorMenu::BUTTON_UP_FRAME);
		}
	}
}
