#ifndef POWERUP_H
#define POWERUP_H
#define WIN32_LEAN_AND_MEAN
#include "entity.h"
#include "constants.h"
#include "audio.h"

namespace garrethPowerUP
{
	const int WIDTH = 32;                   // image width
	const int HEIGHT = 32;                  // image height
	const int X = 0;   // location on screen
	const int Y = 0;
	const int   TEXTURE_COLS = 7;           // texture has 8 columns

	//Sprite Sheet
	const int   PICKUP_START_FRAME = 0;      // Player1 starts at frame 0
	const int   PICKUP_END_FRAME = 6;        // Player1 animation frames 0,1,2,3,4
	const float PICKUP_ANIMATION_DELAY = 0;    // time between frames

	//Right
	const int   SPEED_START_FRAME = 0;      // Player1 starts at frame 0
	const int   SPEED_END_FRAME = 0;        // Player1 animation frames 0,1,2,3,4

	const int   SPEEDDOWN_START_FRAME = 1;      // Player1 starts at frame 0
	const int   SPEEDDOWN_END_FRAME = 1;        // Player1 animation frames 0,1,2,3,4

	const int POWER_START_FRAME = 2;
	const int POWER_END_FRAME = 2;

	const int POWERDOWN_START_FRAME = 3;
	const int POWERDOWN_END_FRAME = 3;

	const int BOMB_START_FRAME = 4;
	const int BOMB_END_FRAME = 4;

	const int BOMBDOWN_START_FRAME = 5;
	const int BOMBDOWB_END_FRAME = 5;

	const int FUSE_START_FRAME = 6;
	const int FUSE_END_FRAME = 6;

	const int FUSEDOWN_START_FRAME = 7;
	const int FUSEDOWB_END_FRAME = 7;

	const int ARMOUR_START_FRAME = 8;
	const int ARMOUR_END_FRAME = 8;

	const int CRYSTAL_START_FRAME = 9;
	const int CRYSTAL_END_FRAME = 9;
}
struct PowerUpStc
{
	float X, Y;
	bool active;
};
class PowerUp :public Entity
{
private:
	int ID;
public:
	PowerUp();
	PowerUp(int &name);
	virtual void draw();
	virtual bool initialize(Game *gamePtr, int width, int height, int ncols,
		TextureManager *textureM);
	void update(float frameTime);

	int getID(){ return ID; }
	void setID(int p){ ID = p; }
	
	//Networking functions
	PowerUpStc getNetData();
	void setNetData(PowerUpStc ps);
};

#endif