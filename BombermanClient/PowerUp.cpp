
#include "PowerUp.h"
#include "constants.h"

PowerUp::PowerUp() :Entity(){
	spriteData.width = garrethPowerUP::WIDTH;           // size of Player
	spriteData.height = garrethPowerUP::HEIGHT;
	spriteData.x = garrethPowerUP::X;                   // location on screen
	spriteData.y = garrethPowerUP::Y;
	spriteData.rect.bottom = garrethPowerUP::HEIGHT;    // rectangle to select parts of an image
	spriteData.rect.right = garrethPowerUP::WIDTH;
	velocity.x = 0;                             // velocity X
	velocity.y = 0;                             // velocity Y
	frameDelay = garrethPowerUP::PICKUP_ANIMATION_DELAY;
	startFrame = garrethPowerUP::PICKUP_START_FRAME;  
	endFrame = garrethPowerUP::PICKUP_END_FRAME;
	currentFrame = startFrame;
	collisionType = entityNS::BOX;
	active = false;
	visible = false;
	//collisionType = entityNS::CIRCLE;
	setLoop(false);
}

PowerUp::PowerUp(int &p) :Entity(){
	spriteData.x = garrethPowerUP::X;              // location on screen
	spriteData.y = garrethPowerUP::Y;
	setCurrentFrame(startFrame);
	collisionType = entityNS::BOX;
	ID = p;
}

bool PowerUp::initialize(Game *gamePtr, int width, int height, int ncols,
	TextureManager *textureM)
{
	//idle.initialize()
	return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}

//=============================================================================
// draw the ship
//=============================================================================
void PowerUp::draw()
{
	Image::draw();              // draw Player

}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void PowerUp::update(float frameTime)
{
	if (ID == 1){
		currentFrame = garrethPowerUP::SPEED_START_FRAME;
		endFrame = garrethPowerUP::SPEED_END_FRAME;
	}
	else if (ID == 2){
		currentFrame = garrethPowerUP::SPEEDDOWN_START_FRAME;
		endFrame = garrethPowerUP::SPEEDDOWN_END_FRAME;
	}
	else if (ID == 3){
		currentFrame = garrethPowerUP::POWER_START_FRAME;
		endFrame = garrethPowerUP::POWER_END_FRAME;
	}
	else if (ID == 4){
		currentFrame = garrethPowerUP::POWERDOWN_START_FRAME;
		endFrame = garrethPowerUP::POWERDOWN_END_FRAME;
	}
	else if (ID == 5){
		currentFrame = garrethPowerUP::BOMB_START_FRAME;
		endFrame = garrethPowerUP::BOMB_END_FRAME;
	}
	else if (ID == 6){
		currentFrame = garrethPowerUP::BOMBDOWN_START_FRAME;
		endFrame = garrethPowerUP::BOMBDOWB_END_FRAME;
	}
	else if (ID == 7){
		currentFrame = garrethPowerUP::FUSE_START_FRAME;
		endFrame = garrethPowerUP::FUSE_END_FRAME;
	}
	else if (ID == 8){
		currentFrame = garrethPowerUP::FUSEDOWN_START_FRAME;
		endFrame = garrethPowerUP::FUSEDOWB_END_FRAME;
	}
	else if (ID == 9){
		currentFrame = garrethPowerUP::ARMOUR_START_FRAME;
		endFrame = garrethPowerUP::ARMOUR_END_FRAME;
	}
	else{
		currentFrame = garrethPowerUP::CRYSTAL_START_FRAME;
		endFrame = garrethPowerUP::CRYSTAL_END_FRAME;
	}
	Entity::update(frameTime);

}

//=======================================
//Networking
//======================================
