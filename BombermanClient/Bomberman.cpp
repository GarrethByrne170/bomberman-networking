// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// createThisClass.cpp v1.1
// This class is the core of the game

#include "Bomberman.h"
using namespace Conor;

//=============================================================================
// Constructor
//=============================================================================
Bomberman::Bomberman()
{

	mapX = garreth::MAP_POSITIONX;
	mapY = garreth::MAP_POSITIONY;
	isPlaying = false;
	countDownOn = false;
    dxFont = new TextDX();  // DirectX font
	fontScore = new TextDX();
    messageY = 0;
	countDownTimer = garreth::ROUND_COUNT_DOWN;
	roundOver = false;
	state = START_OPTION;
	pointer.setX(1100);
	pointer.setY(400);
}

//=============================================================================
// Destructor
//=============================================================================
Bomberman::~Bomberman()
{
	menuOn = true;
	controlsOn = true;
    releaseAll();           // call onLostDevice() for every graphics item
    safeDelete(dxFont);
	safeDelete(fontScore);

}

//=============================================================================
// Initializes the game
// Throws GameError on error
//=============================================================================
void Bomberman::initialize(HWND hwnd)
{
	Game::initialize(hwnd); // throws GameError

	// menu texture
	if (!menuTextures.initialize(graphics, MENU_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menu texture"));
	// menu texture
	if (!controlTextures.initialize(graphics, CONTROLS_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menu texture"));
	// MENU SPRITE
	if (!menuSpriteTexture.initialize(graphics, START))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menu textures"));
	if (!menuSpriteTexture2.initialize(graphics, CONTROLS))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menu textures"));
	if (!menuSpriteTexture3.initialize(graphics, QUIT))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menu textures"));
	if (!menuSpriteTexture4.initialize(graphics, BACK))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menu textures"));
	if (!pointerTextures.initialize(graphics, POINTER_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menu textures"));
	if (!player1Texture.initialize(graphics, BOMBERMAN_PLAYER1))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menu textures"));
	if (!player2Texture.initialize(graphics, BOMBERMAN_PLAYER2))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menu textures"));
	// tile textures
	if (!tileTextures.initialize(graphics, TILES))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing tile textures"));

	// main game textures
	if (!playerTextures.initialize(graphics, PLAYERS_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing game textures"));
	if (!player2Textures.initialize(graphics, PLAYER2_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing game textures"));

	if (!bombTexture.initialize(graphics, BOMB_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing game textures"));

	if (!dBlockTexture.initialize(graphics, DESBLOCK_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing game textures"));

	if (!powerUpTexture.initialize(graphics, POWERUPBLOCK_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing game textures"));

	if (!explosionTexture.initialize(graphics, EXPLOSION_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing game textures"));

	if (!explosionTexture2.initialize(graphics, MID_EXPLOSION_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing game textures"));

	if (!explosionTexture3.initialize(graphics, LARGE_EXPLOSION_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing game textures"));
	// tile image
	if (!tile.initialize(graphics, 0, 0, 0, &tileTextures))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing tile"));

	// menu image
	if (!menu.initialize(graphics, 0, 0, 0, &menuTextures))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menu"));
	// menu image
	if (!controls.initialize(graphics, 0, 0, 0, &controlTextures))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menu"));
	// menu image
	if (!pointer.initialize(graphics, 0, 0, 0, &pointerTextures))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menu"));
	if (!Player1Image.initialize(graphics, 0, 0, 0, &player1Texture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing player 1 image"));
	if (!Player2Image.initialize(graphics, 0, 0, 0, &player2Texture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing player 2 image"));
	Player1Image.setY(200);
	Player2Image.setY(200);
	Player2Image.setX(1100);

	//fonts
	if (fontScore->initialize(graphics, 48, false, false, "Arial") == false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing DirectX font"));
	if (dxFont->initialize(graphics, 48, false, false, "Arial") == false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing DirectX font"));
	roundTimeFont.initialize(graphics, 100, true, false, "Arial");

	roundTimeFont.setFontColor(graphicsNS::RED);
	

	mapIn.open(MAP);
	if (!mapIn)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error loading map data"));
	for (int row = 0; row < MAP_HEIGHT; row++)
	{
		for (int col = 0; col < MAP_WIDTH; col++)
			mapIn >> tileMap[row][col];

	}

	//Menu
	menuSprite.initialize(graphics, &menuSpriteTexture, input, hwnd, 1200, 400, 1.5f, true);
	menuSprite2.initialize(graphics, &menuSpriteTexture2, input, hwnd, 1200, 500, 1.5f, true);
	menuSprite3.initialize(graphics, &menuSpriteTexture3, input, hwnd, 1200, 600, 1.5f, true);
	menuSprite4.initialize(graphics, &menuSpriteTexture4, input, hwnd, 750, 400, 1.5f, true);

	//Player
	if (!player1.initialize(this, Conor::WIDTH, Conor::HEIGHT, Conor::TEXTURE_COLS, &playerTextures))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing player1"));
	player1.setFrames(Conor::SPRITE_START_FRAME, Conor::SPRITE_END_FRAME);
	player1.setCurrentFrame(Conor::IDLE_FRAME);
	player1.setVelocity(VECTOR2(Conor::SPEED, -Conor::SPEED)); // VECTOR2(X, Y)
	player1.setKeys(PLAYER1_RIGHT_KEY, PLAYER1_LEFT_KEY, PLAYER1_UP_KEY, PLAYER1_DOWN_KEY, PLAYER1_DROP_KEY);
	player1.setGamepad(0);
	player1.setEdge(COLLISION_BOX);
	player1.setID(1);
	//player1.setCollisionRadius(Conor::RADUIS);
	//Player2
	if (!player2.initialize(this, Conor::WIDTH, Conor::HEIGHT, Conor::TEXTURE_COLS, &player2Textures))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing player2"));
	player2.setFrames(Conor::SPRITE_START_FRAME, Conor::SPRITE_END_FRAME);
	player2.setCurrentFrame(Conor::IDLE_FRAME);


	

	player2.setVelocity(VECTOR2(Conor::SPEED, -Conor::SPEED)); // VECTOR2(X, Y)
	player2.setKeys(PLAYER2_RIGHT_KEY, PLAYER2_LEFT_KEY, PLAYER2_UP_KEY, PLAYER2_DOWN_KEY, PLAYER2_DROP_KEY);
	player2.setGamepad(1);
	player2.setEdge(COLLISION_BOX);
	player2.setID(2);

	tileEntity.initialize(this, TILE_SIZE, TILE_SIZE, 1, &tileTextures);
	tileEntity.setEdge(garreth::TILE_EDGE);  // for tile collision
	tileEntity.setCollisionType(entityNS::BOX);

	//loads the level of blocks in the game
	//loadLevel();


	for (int i = 0; i < garreth::NUMBER_OF_POWERUPS; i++){
		if (!powerUps[i].initialize(this, 32, 32, 10, &powerUpTexture)){
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing dblock"));
		}
		int a = 0;
		int count = 0;
		int end = 32;
		powerUps[i].setEdge(garreth::POWER_UP_EDGE);
		powerUps[a].setFrames(count, end);
		powerUps[i].setID(i + 1);
		powerUps[i].setActive(false);
		powerUps[i].setVisible(false);
		count += 32;
		end += 32;
		a++;
		if (a > 10){
			a = 0;
		}
	}

	
		if (!bomb.initialize(this, ConorBomb::WIDTH, ConorBomb::HEIGHT, ConorBomb::TEXTURE_COLS, &bombTexture))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing bomb"));
		bomb.setEdge(garreth::TILE_EDGE);
		bomb.setFrames(ConorBomb::BOMB_START_FRAME, ConorBomb::BOMB_END_FRAME);


	
	
		//player 2 bombs
		if (!bomb2.initialize(this, ConorBomb::WIDTH, ConorBomb::HEIGHT, ConorBomb::TEXTURE_COLS, &bombTexture))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing bomb"));
		bomb2.setEdge(garreth::TILE_EDGE);
		bomb2.setFrames(ConorBomb::BOMB_START_FRAME, ConorBomb::BOMB_END_FRAME);

	
		if (!explode.initialize(this, 0,0, 0, &explosionTexture))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing explosion"));
		explode.setCollisionRadius(garrethExplosion::COLLISION_RADIUS);
		if (!explode2.initialize(this, garrethExplosion::MID_WIDTH, garrethExplosion::MID_HEIGHT, 0, &explosionTexture2))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing explosion"));
		explode2.setCollisionRadius(garrethExplosion::MID_COLLISION_RADIUS);
		if (!explode3.initialize(this, garrethExplosion::LARGE_WIDTH,garrethExplosion::LARGE_HEIGHT, 0, &explosionTexture3))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing explosion"));
		explode3.setCollisionRadius(garrethExplosion::LARGE_COLLISION_RADIUS);

		//player 2
		if (!explodeSmallp2.initialize(this, 0, 0, 0, &explosionTexture))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing explosion"));
		explodeSmallp2.setCollisionRadius(garrethExplosion::COLLISION_RADIUS);

		if (!explodeMidp2.initialize(this, garrethExplosion::MID_WIDTH, garrethExplosion::MID_HEIGHT, 0, &explosionTexture2))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing explosion"));
		explodeMidp2.setCollisionRadius(garrethExplosion::MID_COLLISION_RADIUS);

		if (!explodeLargep2.initialize(this, garrethExplosion::LARGE_WIDTH, garrethExplosion::LARGE_HEIGHT, 0, &explosionTexture3))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing explosion"));
		explodeLargep2.setCollisionRadius(garrethExplosion::LARGE_COLLISION_RADIUS);
    return;
}

//=============================================================================
// Update all game items
//=============================================================================
void Bomberman::update()
{

	//Menu Musical Score
	//audio->playCue(MUSIC2);
	if (isPlaying){
		countDownTimer -= frameTime;
		if (countDownTimer <= 0)
			countDownOn = false;
	}
	if (isPlaying && !countDownOn){
		player1.update(frameTime);
		player2.update(frameTime);
		collisions();

		//update explosions
		explode.update(frameTime);
		explode2.update(frameTime);
		explode3.update(frameTime);
		explodeLargep2.update(frameTime);
		explodeMidp2.update(frameTime);
		explodeSmallp2.update(frameTime);
		pickUp = rand() % 8 - 1;
		if (!countDownOn){
			handleTimer(frameTime);
		}
		//check to see what explosions trigger
		
			if (player1.getBombPower() == 1 ){
				if (bomb.getFuse() < 0 && bomb.getFuse() > -0.2f){
					explode.explode(&bomb);

				}
			}
			else if (player1.getBombPower() == 2){
				if (bomb.getFuse() < 0 && bomb.getFuse() > -0.2f){
					explode2.explode(&bomb);
				}

			}
			else if ((player1.getBombPower() == 3)){
				if (bomb.getFuse() < 0 && bomb.getFuse() > -0.2f){
					explode3.explode(&bomb);
				}
			}


			if (player2.getBombPower() == 1){
				if (bomb2.getFuse() < 0 && bomb2.getFuse() > -0.2f){
					explodeSmallp2.explode(&bomb2);
				}
			}
			else if (player2.getBombPower() == 2){
				if (bomb2.getFuse() < 0 && bomb2.getFuse() > -0.2f){
					explodeMidp2.explode(&bomb2);
				}

			}
			else if ((player2.getBombPower() == 3)){
				if (bomb2.getFuse() < 0 && bomb2.getFuse() > -0.2f){
					explodeLargep2.explode(&bomb2);
				}
			}
		
		for (int i = 0; i < garreth::NUMBER_OF_DBLOCKS; i++){
			if (dBlocks[i].getActive()){
				dBlocks[i].update(frameTime);
			}
		}
		for (int i = 0; i < garreth::NUMBER_OF_POWERUPS; i++){
			powerUps[i].update(frameTime);
		}

		if (input->wasKeyPressed(PLAYER1_DROP_KEY) || input->getGamepadA(0)){
			if (player1.getActive()){
				handleBombs(&player1);
			}
		}
		if (input->wasKeyPressed(PLAYER2_DROP_KEY) || input->getGamepadA(1)){
			if (player2.getActive()){
				handleBombs(&player2);
			}
		}
		bomb.update(frameTime);
		bomb2.update(frameTime);
		if (player1.getIsDead() || player2.getIsDead() ||  roundCount == 3){
			roundOver = true;
		}
		if (roundOver){
			nextRoundTimer -= frameTime;
			if (nextRoundTimer <= 0){
				roundStart();
			}
		}
		
	}//end isplaying
	if (menuOn)
	{
		
		handleMenuControls(frameTime);

		if (menuSprite.getSwitchOn())    // if push button switch on
		{
			if (menuOn)
			{
				
				controlsOn = false;
				menuOn = false;
				isPlaying = true;
				countDownOn = true;
				input->clearAll();
				roundStart();
			}
		}
		if (menuSprite2.getSwitchOn())
		{

			if (controlsOn)
			{
				menuOn = false;
				controlsOn = true;
				input->clearAll();
				pointerChange();
				//state = BACK_OPTION;
			}
		}
		if (menuSprite3.getSwitchOn())
		{
			state = EXIT_OPTION;
			PostQuitMessage(0);
		}
	}
	if (controlsOn)
	{
		if (menuSprite4.getSwitchOn())
		{
			menuOn = true;
			input->clearAll();
		}
	}
	
	
	
	menuSprite.update(frameTime);
	menuSprite2.update(frameTime);
	menuSprite3.update(frameTime);
	menuSprite4.update(frameTime);
}

//=============================================================================
// Artificial Intelligence
//=============================================================================
void Bomberman::ai()
{}

//=============================================================================
// Handle collisions
//=============================================================================
void Bomberman::collisions()
{
	VECTOR2 collisionVector;
	if (player1.collidesWith(player2, collisionVector))
	{
		player1.toOldPosition();      // move ship out of collision	
	
	}
	if (player2.collidesWith(player1, collisionVector))
	{
		player2.toOldPosition();      // move ship out of collision		
	}
	
	
		
	handleTileCollision(collisionVector);

	//checks to see if the explsoions collide with the blocks
	for (int i = 0; i < garreth::NUMBER_OF_DBLOCKS; i++){
		if (explode.collidesWith(dBlocks[i], collisionVector) || explode2.collidesWith(dBlocks[i], collisionVector) || explode3.collidesWith(dBlocks[i], collisionVector)
			|| explodeSmallp2.collidesWith(dBlocks[i], collisionVector) || explodeMidp2.collidesWith(dBlocks[i], collisionVector) || explodeLargep2.collidesWith(dBlocks[i], collisionVector)){
		
			dBlocks[i].setActive(false);
			dBlocks[i].setVisible(false);
			//spawn a random power up if its not active already
			if (!powerUps[pickUp].getActive()){
				powerUps[pickUp].setX(dBlocks[i].getX());
				powerUps[pickUp].setY(dBlocks[i].getY());
				powerUps[pickUp].setActive(true);
				powerUps[pickUp].setVisible(true);
			}
		}

		if (dBlocks[i].collidesWith(player2, collisionVector)){
			player2.toOldPosition();
			
		}
		if (dBlocks[i].collidesWith(player1, collisionVector)){
			player1.toOldPosition();

		}
	}
	//checks to see if player1 collides with a power up
	for (int i = 0; i < garreth::NUMBER_OF_POWERUPS; i++){
		if (player1.collidesWith(powerUps[i], collisionVector)){
			player1.addPowerUP(powerUps[i]);
		powerUps[i].setActive(false);
		powerUps[i].setVisible(false);
		audio->playCue(PICKUP);
		
		}
		//checks if player2 collides with a pick up
		if (player2.collidesWith(powerUps[i], collisionVector)){
			player2.addPowerUP(powerUps[i]);
			powerUps[i].setActive(false);
			powerUps[i].setVisible(false);
			audio->playCue(PICKUP);
			
			
		}
	}

	if (player1.collidesWith(bomb, collisionVector)){
		player1.toOldPosition();
	}

	if (player2.collidesWith(bomb, collisionVector)){
		player2.toOldPosition();
	}

	if (player1.collidesWith(bomb2, collisionVector)){
		player1.toOldPosition();
	}

	if (player2.collidesWith(bomb2, collisionVector)){
		player2.toOldPosition();
	}

	//checks to see if there is a collsion with player1 and its own explosions
	if (player1.collidesWith(explode, collisionVector) || player1.collidesWith(explode2, collisionVector) || player1.collidesWith(explode3, collisionVector)){		
			explode.setActive(false);
			explode2.setActive(false);
			explode3.setActive(false);
				player1.setScore(player1.getScore() - 10);
				player1.setActive(false);
				player1.setVisible(false);
				player1.setIsDead(true);
				audio->playCue(DIED);
				input->gamePadVibrateLeft(0, 40000, 1.0);
				input->gamePadVibrateRight(0, 40000, 1.0);
				

	}
	//checks to see if there is a collsion with player2 and player1 explosions
	if (player2.collidesWith(explode, collisionVector) || player2.collidesWith(explode2, collisionVector) || player2.collidesWith(explode3, collisionVector)){
		
			explode.setActive(false);
			explode2.setActive(false);
			explode3.setActive(false);
		
			player1.setScore(player1.getScore() + 10);
			player2.setActive(false);
			player2.setVisible(false);
			player2.setIsDead(true);
			audio->playCue(DIED);
			input->gamePadVibrateLeft(1, 40000, 1.0);
			input->gamePadVibrateRight(1, 40000, 1.0);
		
	}
	//checks to see if there is a collsion with player2 and its own explosions
	if (player2.collidesWith(explodeSmallp2, collisionVector) || player2.collidesWith(explodeMidp2, collisionVector)
		|| player2.collidesWith(explodeLargep2, collisionVector)){	
			explodeSmallp2.setActive(false);
			explodeMidp2.setActive(false);
			explodeLargep2.setActive(false);			
			player2.setScore(player2.getScore() - 10);
			player2.setActive(false);
			player2.setVisible(false);
			player2.setIsDead(true);
			audio->playCue(DIED);
			input->gamePadVibrateLeft(1, 40000, 1.0);
			input->gamePadVibrateRight(1, 40000, 1.0);
		
	}
	//checks to see if there is a collsion with player1 and its player2 explosions
	if (player1.collidesWith(explodeSmallp2, collisionVector) || player1.collidesWith(explodeMidp2, collisionVector) || player1.collidesWith(explodeLargep2, collisionVector)){
	
			explodeSmallp2.setActive(false);
			explodeMidp2.setActive(false);
			explodeLargep2.setActive(false);	
			player2.setScore(player2.getScore() + 10);
			player1.setActive(false);
			player1.setVisible(false);
			player1.setIsDead(true);
			audio->playCue(DIED);
			input->gamePadVibrateLeft(0, 40000, 1.0);
			input->gamePadVibrateRight(0, 40000, 1.0);
	}
}
//=============================================================================
// Name: handleTileCollision
// Description: handles collision for the tile map (from Tile Collision )
// Parameters: Vector2 collsionVector
// Returns: none
// Garreth
//=============================================================================
void Bomberman::handleTileCollision(VECTOR2 collisionVector){
	UINT player1Row = static_cast<UINT>((player1.getY() - mapY) / TILE_SIZE);
	UINT player1Col = static_cast<UINT>((player1.getX() - mapX) / TILE_SIZE);

	if (player1Col > 0 && player1Row < MAP_HEIGHT && tileMap[player1Row][player1Col - 1] <= garreth::MAX_COLLISION_TILE)
	{
		tileEntity.setX(static_cast<float>((player1Col - 1)*TILE_SIZE + mapX));    // position tileEntity
		tileEntity.setY(static_cast<float>(player1Row*TILE_SIZE + mapY));

		tileEntity.setEdge(COLLISION_BOX);
		// if collision between player and tile
		if (player1.collidesWith(tileEntity, collisionVector))
			player1.toOldPosition();
	}
	// if current tile may be collided with
	if (player1Col >= 0 && player1Row < MAP_HEIGHT && tileMap[player1Row][player1Col] <= garreth::MAX_COLLISION_TILE)
	{
		tileEntity.setX(static_cast<float>(player1Col*TILE_SIZE + mapX));     // position tileEntity
		tileEntity.setY(static_cast<float>(player1Row*TILE_SIZE + mapY));
		tileEntity.setEdge(COLLISION_BOX);
		// if collision between player and tile
		if (player1.collidesWith(tileEntity, collisionVector))
			player1.toOldPosition();
	}
	// if tile right of player may be collided with
	if (player1Col < MAP_WIDTH - 1 && player1Row < MAP_HEIGHT && tileMap[player1Row][player1Col + 1] <= garreth::MAX_COLLISION_TILE)
	{
		tileEntity.setX(static_cast<float>((player1Col + 1)*TILE_SIZE + mapX));   // position tileEntity
		tileEntity.setY(static_cast<float>(player1Row*TILE_SIZE + mapY));
		tileEntity.setEdge(COLLISION_BOX);
		// if collision between player and tile
		if (player1.collidesWith(tileEntity, collisionVector))
			player1.toOldPosition();
	}
	// if tile bottom left of player may be collided with
	if (player1Col > 0 && player1Row < MAP_HEIGHT - 1 && tileMap[player1Row + 1][player1Col - 1] <= garreth::MAX_COLLISION_TILE)
	{
		tileEntity.setX(static_cast<float>((player1Col - 1)*TILE_SIZE + mapX));   // position tileEntity
		tileEntity.setY(static_cast<float>((player1Row + 1)*TILE_SIZE + mapY));
		tileEntity.setEdge(COLLISION_BOX);
		// if collision between player and tile
		if (player1.collidesWith(tileEntity, collisionVector))
			player1.toOldPosition();
	}
	// if tile below player may be collided with
	if (player1Row < MAP_HEIGHT - 1 && tileMap[player1Row + 1][player1Col] <= garreth::MAX_COLLISION_TILE)
	{
		tileEntity.setX(static_cast<float>(player1Col*TILE_SIZE + mapX));     // position tileEntity
		tileEntity.setY(static_cast<float>((player1Row + 1)*TILE_SIZE + mapY));
		tileEntity.setEdge(COLLISION_BOX);
		// if collision between player and tile
		if (player1.collidesWith(tileEntity, collisionVector))
			player1.toOldPosition();
	}
	// if tile bottom right of player may be collided with
	if (player1Col < MAP_WIDTH - 1 && player1Row < MAP_HEIGHT - 1 && tileMap[player1Row + 1][player1Col + 1] <= garreth::MAX_COLLISION_TILE)
	{
		tileEntity.setX(static_cast<float>((player1Col + 1)*TILE_SIZE + mapX));   // position tileEntity
		tileEntity.setY(static_cast<float>((player1Row + 1)*TILE_SIZE + mapY));
		tileEntity.setEdge(COLLISION_BOX);
		// if collision between player and tile
		if (player1.collidesWith(tileEntity, collisionVector))
			player1.toOldPosition();
	}
	//======================================================================
	//Player 2
	//======================================================================
	UINT player2Row = static_cast<UINT>((player2.getY() - mapY) / TILE_SIZE);
	UINT player2Col = static_cast<UINT>((player2.getX() - mapX) / TILE_SIZE);
	if (player2Col > 0 && player2Row < MAP_HEIGHT && tileMap[player2Row][player2Col - 1] <= garreth::MAX_COLLISION_TILE)
	{
		tileEntity.setX(static_cast<float>((player2Col - 1)*TILE_SIZE + mapX));    // position tileEntity
		tileEntity.setY(static_cast<float>(player2Row*TILE_SIZE + mapY));

		tileEntity.setEdge(COLLISION_BOX);
		// if collision between player and tile
		if (player2.collidesWith(tileEntity, collisionVector))
			player2.toOldPosition();
	}
	// if current tile may be collided with
	if (player2Col >= 0 && player2Row < MAP_HEIGHT && tileMap[player2Row][player2Col] <= garreth::MAX_COLLISION_TILE)
	{
		tileEntity.setX(static_cast<float>(player2Col*TILE_SIZE + mapX));     // position tileEntity
		tileEntity.setY(static_cast<float>(player2Row*TILE_SIZE + mapY));
		tileEntity.setEdge(COLLISION_BOX);
		// if collision between player and tile
		if (player2.collidesWith(tileEntity, collisionVector))
			player2.toOldPosition();
	}
	// if tile right of player may be collided with
	if (player2Col < MAP_WIDTH - 1 && player2Row < MAP_HEIGHT && tileMap[player2Row][player2Col + 1] <= garreth::MAX_COLLISION_TILE)
	{
		tileEntity.setX(static_cast<float>((player2Col + 1)*TILE_SIZE + mapX));   // position tileEntity
		tileEntity.setY(static_cast<float>(player2Row*TILE_SIZE + mapY));
		tileEntity.setEdge(COLLISION_BOX);
		// if collision between player and tile
		if (player2.collidesWith(tileEntity, collisionVector))
			player2.toOldPosition();
	}
	// if tile bottom left of player may be collided with
	if (player2Col > 0 && player2Row < MAP_HEIGHT - 1 && tileMap[player2Row + 1][player2Col - 1] <= garreth::MAX_COLLISION_TILE)
	{
		tileEntity.setX(static_cast<float>((player2Col - 1)*TILE_SIZE + mapX));   // position tileEntity
		tileEntity.setY(static_cast<float>((player2Row + 1)*TILE_SIZE + mapY));
		tileEntity.setEdge(COLLISION_BOX);
		// if collision between player and tile
		if (player2.collidesWith(tileEntity, collisionVector))
			player2.toOldPosition();
	}
	// if tile below player may be collided with
	if (player2Row < MAP_HEIGHT - 1 && tileMap[player2Row + 1][player2Col] <= garreth::MAX_COLLISION_TILE)
	{
		tileEntity.setX(static_cast<float>(player2Col*TILE_SIZE + mapX));     // position tileEntity
		tileEntity.setY(static_cast<float>((player2Row + 1)*TILE_SIZE + mapY));
		tileEntity.setEdge(COLLISION_BOX);
		// if collision between player and tile
		if (player2.collidesWith(tileEntity, collisionVector))
			player2.toOldPosition();
	}
	// if tile bottom right of player may be collided with
	if (player2Col < MAP_WIDTH - 1 && player2Row < MAP_HEIGHT - 1 && tileMap[player2Row + 1][player2Col + 1] <= garreth::MAX_COLLISION_TILE)
	{
		tileEntity.setX(static_cast<float>((player2Col + 1)*TILE_SIZE + mapX));   // position tileEntity
		tileEntity.setY(static_cast<float>((player2Row + 1)*TILE_SIZE + mapY));
		tileEntity.setEdge(COLLISION_BOX);
		// if collision between player and tile
		if (player2.collidesWith(tileEntity, collisionVector))
			player2.toOldPosition();
	}
}

//=============================================================================
// Render game items
//=============================================================================
void Bomberman::render()
{
	graphics->spriteBegin();
	//Draw the tile map
	for (int row = 0; row<MAP_HEIGHT; row++)       // for each row of map
	{
		tile.setY((float)(row*TILE_SIZE)); // set tile Y
		for (int col = 0; col<MAP_WIDTH; col++)    // for each column of map
		{
			if (tileMap[row][col] >= 0)          // if tile present
			{
				tile.setX((float)(col*(TILE_SIZE)) + mapX);  // set tile X
				tile.setY((float)(row*(TILE_SIZE)) + mapY);  // set tile Y
				// if tile on screen
				if ((tile.getX() > -TILE_SIZE && tile.getX() < GAME_WIDTH) &&
					(tile.getY() > -TILE_SIZE && tile.getY() < GAME_HEIGHT))
					tile.draw(UINT(tileMap[row][col]));             // draw tile
			}
		}
	}

	

	player1.draw();
	player2.draw();
	//draw the side images
	Player1Image.draw();
	Player2Image.draw();
	//draw the destructible blocks
	for (int i = 0; i < garreth::NUMBER_OF_DBLOCKS; i++){
		if (dBlocks[i].getActive()){
			dBlocks[i].draw();
		}
	}
	//draw the powerups
	for (int i = 0; i < garreth::NUMBER_OF_POWERUPS; i++){
		powerUps[i].draw();
	}
	
	bomb.draw();
	bomb2.draw();
	explode.draw();
	explode2.draw();
	explode3.draw();
	explodeLargep2.draw();
	explodeSmallp2.draw();
	explodeMidp2.draw();
	//Text 
	fontScore->print("Player1\n    " + std::to_string(player1.getScore()), 120, 10);
	   dxFont->print("Player2\n    " + std::to_string(player2.getScore()) , 1300, 10);
	   if (!roundOver){
		   roundTimeFont.print(std::to_string(min) + ":" + std::to_string((int)sec), 700, 10);
	   }
	if (countDownOn)
	{	
		roundTimeFont.print(std::to_string((int)countDownTimer), GAME_WIDTH / 2, 300);
	}
	
	if (controlsOn)
	{
		controls.draw();
		menuSprite4.draw();
		//pointer.draw();
	}
	if (menuOn)
	{

		menu.draw();
		menuSprite.draw();
		menuSprite2.draw();
		menuSprite3.draw();
		pointer.draw();
		audio->playCue(MENU);
	}
	
		
	graphics->spriteEnd(); 
}

//=============================================================================
// The graphics device was lost.
// Release all reserved video memory so graphics device may be reset.
//=============================================================================
void Bomberman::releaseAll()
{
	playerTextures.onLostDevice();
	menuTextures.onLostDevice();
	menuSpriteTexture.onLostDevice();
	menuSpriteTexture2.onLostDevice();
	menuSpriteTexture3.onLostDevice();
	menuSpriteTexture4.onLostDevice();
	pointerTextures.onLostDevice();
	controlTextures.onLostDevice();
	tileTextures.onLostDevice();
	dBlockTexture.onLostDevice();
	bombTexture.onLostDevice();
	powerUpTexture.onLostDevice();
	explosionTexture.onLostDevice();
	SAFE_ON_LOST_DEVICE(fontScore);
	SAFE_ON_LOST_DEVICE(dxFont);
    Game::releaseAll();
    return;
}

//=============================================================================
// The grahics device has been reset.
// Recreate all surfaces.
//=============================================================================
void Bomberman::resetAll()
{
	SAFE_ON_RESET_DEVICE(fontScore);
	SAFE_ON_RESET_DEVICE(dxFont);
	playerTextures.onResetDevice();
	menuTextures.onResetDevice();
	menuSpriteTexture.onResetDevice();
	menuSpriteTexture2.onResetDevice();
	menuSpriteTexture3.onResetDevice();
	menuSpriteTexture4.onResetDevice();
	pointerTextures.onResetDevice();
	controlTextures.onResetDevice();
	tileTextures.onResetDevice();
	dBlockTexture.onResetDevice();
	powerUpTexture.onResetDevice();
	bombTexture.onResetDevice();
	explosionTexture.onResetDevice();
    Game::resetAll();
    return;
}
//=============================================================================
// Name: roundStart
// Description:Resets the game state and variables when the round starts
// Parameters: none
// Returns: none
// Garreth
//=============================================================================
void Bomberman::roundStart()
{
	roundOver = false;
	roundCount = 0;
	sec = 0;
	min = garreth::ROUND_TIME;
	countDownOn = true;
	countDownTimer = garreth::ROUND_COUNT_DOWN;
	nextRoundTimer = garreth::END_ROUND_COUNT_DOWN;
	loadLevel();
	audio->stopCue(MENU);
	audio->playCue(MUSIC1);
	player1.resetStats();
	player2.resetStats();
	player1.setX(garreth::PLAYER1_SPAWNX);
	player1.setY(garreth::PLAYER1_SPAWNY);
	player2.setX(garreth::PLAYER2_SPAWNX);
	player2.setY(garreth::PLAYER2_SPAWNY);
}
//=============================================================================
// Name: handleBombs
// Description: handles the planting of bombs when the player wants to plant a bomb
// Parameters: pointer a player
// Returns: none
// Garreth
//=============================================================================
void Bomberman::handleBombs(Player *player){
	if (player->getID() == 1){
		bomb.plant(player, player->getBombTimer());
	}
	else if (player->getID() == 2){
		bomb2.plant(player, player->getBombTimer());
	}
}


//=============================================================================
// Name: loadLevel
// Description: This function sets the postion of the destructible blocks on the map
// Parameters: none
// Returns: none
// Garreth
//=============================================================================
void Bomberman::loadLevel(){
	// load map
	
	int count = 0;
	int step = 0;
	int stepSize = 0;
	for (int i = 0; i < 7; i++){

		if (!dBlocks[i].initialize(this, 0, 0, 0, &dBlockTexture)){
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing dblock"));
		}

		dBlocks[i].setID(i);
		dBlocks[i].setX(696 + count);
		dBlocks[i].setY(232 + stepSize);
		dBlocks[i].setEdge(garreth::TILE_EDGE);
		count = count + 32;
	}
	count = 0;
	for (int i = 7; i < 16; i++){
		if (!dBlocks[i].initialize(this, 0, 0, 0, &dBlockTexture)){
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing dblock"));
		}
		dBlocks[i].setID(i);
		dBlocks[i].setX(664 + count);
		dBlocks[i].setY(264);
		dBlocks[i].setEdge(garreth::TILE_EDGE);
		count = count + 32;

	}
	count = 0;
	for (int i = 17; i < 72; i++){
		if (!dBlocks[i].initialize(this, 0, 0, 0, &dBlockTexture)){
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing dblock"));
		}
		dBlocks[i].setID(i);
		dBlocks[i].setX(632 + count);
		dBlocks[i].setY(296 + stepSize);
		dBlocks[i].setEdge(garreth::TILE_EDGE);
		count = count + 32;
		if (i == 27 || i == 38 || i == 49 || i == 60 || i == 71){
			stepSize += 32;
			count = 0;
		}
	}
	count = 0;
	for (int i = 73; i < 82; i++){
		if (!dBlocks[i].initialize(this, 0, 0, 0, &dBlockTexture)){
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing dblock"));
		}
		dBlocks[i].setID(i);
		dBlocks[i].setX(664 + count);
		dBlocks[i].setY(456);
		dBlocks[i].setEdge(garreth::TILE_EDGE);
		count = count + 32;

	}
	count = 0;
	for (int i = 83; i < 90; i++){

		if (!dBlocks[i].initialize(this, 0, 0, 0, &dBlockTexture)){
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing dblock"));
		}

		dBlocks[i].setID(i);
		dBlocks[i].setX(696 + count);
		dBlocks[i].setY(488);
		dBlocks[i].setEdge(garreth::TILE_EDGE);
		count = count + 32;
	}

	ResetLevel();
}
//=============================================================================
// Name: ResetLevel
// Description: Resets the blocks, powerups, bombs and explosions when the round ends
// Parameters: none
// Returns: none
// Garreth
//=============================================================================
void Bomberman::ResetLevel(){
	for (int i = 0; i < garreth::NUMBER_OF_DBLOCKS; i++){
		if (!dBlocks[i].getActive()){
			dBlocks[i].setActive(true);
			dBlocks[i].setVisible(true);
		}
	}
	for (int i = 0; i < garreth::NUMBER_OF_POWERUPS; i++){
		if (powerUps[i].getActive()){
			powerUps[i].setActive(false);
			powerUps[i].setVisible(false);
		}
	}
	if (bomb.getActive()){
		bomb.setActive(false);
		bomb.setVisible(false);
	}
	if (bomb2.getActive()){
		bomb2.setActive(false);
		bomb2.setVisible(false);
	}
}
void Bomberman::pointerChange()
{
	if (state == START_OPTION)
	{
		pointer.setX(1100);
		pointer.setY(400);
	}
	if (state == OPTIONS_OPTION)
	{
		pointer.setX(1100);
		pointer.setY(500);
	}
	if (state == EXIT_OPTION)
	{
		pointer.setX(1100);
		pointer.setY(600);

	}
	if (state == BACK_OPTION)
	{
		pointer.setX(680);
		pointer.setY(400);
	}
}
//=============================================================================
// Name: handleTimer
// Description: Handles the round timer for the round
// Parameters: frametime
// Returns: none
// Garreth
//=============================================================================
void Bomberman::handleTimer(float frameTime){
	if ( !roundOver){
		sec -= frameTime;
		if (sec < 0){
			sec = 60;
			min--;
			roundCount++;
		}
	}
}

void Bomberman::handleMenuControls(float frameTime){
	SHORT movementX = input->getGamepadThumbLX(0);
	SHORT movementY = input->getGamepadThumbLY(0);

	if (menuOn || controlsOn)
	{
		if (input->wasKeyPressed(PLAYER2_UP_KEY) || input->getGamepadDPadUp(0) || movementY > 0)
		{
			if (state == START_OPTION){
				state = EXIT_OPTION;
				pointerChange();
			}
			else if (state == OPTIONS_OPTION){
				state = START_OPTION;
				pointerChange();
			}
			else if (state == EXIT_OPTION){
				state = OPTIONS_OPTION;
				pointerChange();
			}
		}
		else if (input->wasKeyPressed(PLAYER2_DOWN_KEY) || input->getGamepadDPadDown(0) || movementY < 0)
		{

			if (state == START_OPTION){
				state = OPTIONS_OPTION;
				pointerChange();
			}
			else if (state == OPTIONS_OPTION){
				state = EXIT_OPTION;
				pointerChange();
			}
			else if (state == EXIT_OPTION){
				state = START_OPTION;
				pointerChange();
			}
		}

		else if (input->isKeyDown(PLAYER1_DROP_KEY) || input->getGamepadA(0))
		{
			if (state == START_OPTION){
				if (menuOn)
				{
					controlsOn = false;
					menuOn = false;
					isPlaying = true;
					countDownOn = true;
					input->clearAll();
					roundStart();
				}
			}
			else	if (state == OPTIONS_OPTION){
				if (controlsOn)
				{

					menuOn = false;
					controlsOn = true;
					input->clearAll();
					state = BACK_OPTION;
					pointerChange();
				}
			}
			else if (state == EXIT_OPTION){
				PostQuitMessage(0);
			}

		}

		else if ( input->getGamepadB(0) && state == BACK_OPTION){
			menuOn = true;
			input->clearAll();
			state = START_OPTION;
			pointerChange();
		}
	}
}

