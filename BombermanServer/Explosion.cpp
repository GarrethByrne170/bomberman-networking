//Garreth Byrne
#include "Explosion.h"


//=============================================================================
// default constructor
//=============================================================================
Explosion::Explosion() : Entity()
{
	spriteData.x = garrethExplosion::X;              // location on screen
	spriteData.y = garrethExplosion::Y;
	//radius = garrethExplosion::COLLISION_RADIUS;
	active = false;
	visible = false;
	collisionType = entityNS::CIRCLE;
	explosionOn = false;
	setLoop(false);
}
void Explosion::update(float frameTime){
	timer -= 1;
	if (explosionOn){	
		visible = true;
		active = true;
	}
	if (timer > 0){
		explosionOn = true;
		visible = false;
		active = false;
	}
	else{
		visible = false;
		active = false;
		explosionOn = false;
	}
	
	
	Entity::update(frameTime);
}
//=============================================================================
// Name: explode
// Description: This function handles the explosion of the bomb
// Parameters: Pointer to the bomb object
// Returns: none
// By Garreth
//=============================================================================
void Explosion::explode(Entity *bomb){
	if (timer <= 0.0f){
		timer = 2;
		active = true;
		visible = true;
		explosionOn = true;
		spriteData.x = bomb->getCenterX() - spriteData.width / 2;
		spriteData.y = bomb->getCenterY() - spriteData.height / 2;
		audio->playCue(EXPLODE);
		
	}
}
//==================================
//Networking
//==================================
ExplosionStc Explosion::getNetData(){
	ExplosionStc data;
	data.X = getX();
	data.Y = getY();
	data.active = active;
	data.timer = timer;
	data.isExplosionOn = explosionOn;
	return data;
}
