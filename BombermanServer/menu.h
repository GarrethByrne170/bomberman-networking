// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// dashboard.h v1.0

#ifndef MENU_H            // Prevent multiple definitions if this 
#define MENU_H            // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include <string>
#include <sstream>
#include "image.h"
#include "constants.h"
#include "textureManager.h"
#include "input.h"

namespace ConorMenu
{
	const int   IMAGE_WIDTH = 100;        // each texture size
	const int   IMAGE_HEIGHT = 50;        // each texture size
	const int   TEXTURE_COLS = 0;       // texture has 4 columns
	const int   SWITCH_OFF_FRAME = 0;
	const int   SWITCH_ON_FRAME = 0;
	const int   BUTTON_UP_FRAME = 0;
	const int   BUTTON_DOWN_FRAME = 0;
	   // width of bar + gap
	enum DialType{ DIAL360, DIAL270, DIAL180 };
}



class MenuSprite : public Image
{
private:
private:
	Input   *input;
	HWND    hwnd;
	RECT    switchRect;         // mouse click region
	bool    switchOn;           // switch state
	bool    mouseClick;         // track mouse clicks
	bool    momentary;          // true for momentary, false for toggle
public:
	// Pushbutton switch constructor
	MenuSprite();
	// Initialize the Pushbutton
	// Pre: *graphics = pointer to Graphics object
	//      *textureM = pointer to TextureManager object
	//      *in = pointer to Input object
	//      hwnd = handle to window
	//      left, top = screen location
	//      scale = scaling (zoom) amount
	//      momentary = true for momentary, false for toggle
	bool initialize(Graphics *graphics, TextureManager *textureM, Input *in, HWND hwnd,
		int left, int top, float scale, bool momentary);
	// Override update
	virtual void update(float frameTime);
	// Get switch state
	bool getSwitchOn()  { return switchOn; }
	// Set switch state
	void setSwitch(bool on) { switchOn = on; }
};



#endif

