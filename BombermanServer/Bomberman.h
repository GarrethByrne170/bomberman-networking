// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// createThisClass.h

#ifndef _CREATETHIS_H           // Prevent multiple definitions if this 
#define _CREATETHIS_H           // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include <string>
#include <sstream>
#include "game.h"
#include "textureManager.h"
#include "image.h"
#include "textDX.h"
#include "player.h"
#include "DestructibleBlock.h"
#include "PowerUp.h"
#include "menu.h"
#include "Explosion.h"
#include "net.h"
using namespace std;


//----------------------------------------------------
// Tilemap
//----------------------------------------------------
namespace Conor
{
	const int TILE_SIZE = 32;
	const int MAP_HEIGHT = 11;
	const int MAP_WIDTH = 13;

}
namespace garreth{
	const USHORT MAX_COLLISION_TILE = 0;
	const int TILE_EDGE_TOP = -TILE_SIZE / 2;  // for tile collision
	const int TILE_EDGE_BOTTOM = TILE_SIZE / 2;
	const int TILE_EDGE_LEFT = -TILE_SIZE / 2;
	const int TILE_EDGE_RIGHT = TILE_SIZE / 2;
	const RECT TILE_EDGE = { TILE_EDGE_LEFT , TILE_EDGE_TOP , TILE_EDGE_RIGHT , TILE_EDGE_BOTTOM  };
	//const RECT TILE_EDGE = { TILE_EDGE_LEFT + 1, TILE_EDGE_TOP + 1, TILE_EDGE_RIGHT + 1, TILE_EDGE_BOTTOM + 1 };
	const RECT POWER_UP_EDGE = { TILE_EDGE_LEFT, TILE_EDGE_TOP, TILE_EDGE_RIGHT, TILE_EDGE_BOTTOM };
	const char FONT[] = "Arial Bold";  // font
	const int FONT_BIG_SIZE = 256;     // font height
	const int FONT_SCORE_SIZE = 48;
	const int NUMBER_OF_POWERUPS = 10;
	const int NUMBER_OF_DBLOCKS = 90;
	const int MAP_POSITIONX = 600;
	const int MAP_POSITIONY = 200;
	const int SCORE = 10;
	const int ROUND_COUNT_DOWN = 3;
	const int END_ROUND_COUNT_DOWN = 3;
	const float PLAYER1_SPAWNX = 632;
	const float PLAYER1_SPAWNY = 232;
	const float PLAYER2_SPAWNX = 950;
	const float PLAYER2_SPAWNY = 488;
	const float ROUND_TIME = 2;
	//=========================================
	//networking
	//========================================
	const int CLIENT = 1;
	const int SERVER = 2;
	const int MAX_PLAYERS = 2;
	//network constants
	const int BUFSIZE = 256;
	const int LEFT_BIT = 0x01;
	const int RIGHT_BIT = 0x02;
	const int FORWARD_BIT = 0x08;
	const int BACKWARDS_BIT = 0x10;
	const int BOMBPLACE_BIT = 0x20;
	
}
//======================================================
//Networking structs
//======================================================
struct ConnectResponse
{
	char response[netNS::RESPONSE_SIZE];
	UCHAR number;
};
//player struct for one player
struct PlayerGameStc
{
	PlayerStc playerData;
	BombStc bombData;
};

//ToclientStc 
struct ToClientStc
{
	PlayerGameStc player[garreth::MAX_PLAYERS];
	UCHAR gamestate;
};

struct toServerStc
{
	UCHAR buttons;
	UCHAR playerN;
};

//=============================================================================
// This class is the core of the game
//=============================================================================
class Bomberman : public Game
{
private:
    // game items
	TextureManager tileTextures;
	TextureManager controlTextures; // textures
    TextureManager menuTextures; // textures
	TextureManager pointerTextures; // textures
	TextureManager player1Texture,player2Texture;
	TextureManager playerTextures,player2Textures;  // game texture
	TextureManager bombTexture;
	TextureManager dBlockTexture;
	TextureManager powerUpTexture;
	TextureManager explosionTexture,explosionTexture2,explosionTexture3;
	TextureManager menuSpriteTexture, menuSpriteTexture2, menuSpriteTexture3, menuSpriteTexture4;
	Player player1, player2;
	MenuSprite menuSprite, menuSprite2, menuSprite3, menuSprite4;
	std::vector<Player> playerlist;
	std::vector<int>::iterator itr;
	//player 1 bombs
	Bomb bomb;
	//player 2 bombs
	Bomb bomb2;
	//player 1 explosions
	Explosion explode,explode2,explode3;
	//Player2 explosions
	Explosion explodeSmallp2, explodeMidp2, explodeLargep2;
	DestructibleBlock dBlocks[garreth::NUMBER_OF_DBLOCKS];

	PowerUp powerUps[garreth::NUMBER_OF_POWERUPS];
	//Bomb bombs[3];
	Image tile;
    Image   menu;               // menu image
	Image pointer;
	Image Player1Image,Player2Image;
	Image controls;
    TextDX  *dxFont;            // DirectX font
	TextDX *fontScore;
	TextDX roundTimeFont;
	Bomb *b;
	Entity tileEntity;
	std::ifstream mapIn;        // used to read maps
	float   mapX, mapY;
    string  message;
    float messageY;
	bool    menuOn;
	bool controlsOn;
	bool countDownOn;
	bool isPlaying;
	bool roundOver;
	float countDownTimer;
	int pickUp;
	USHORT tileMap[Conor::MAP_HEIGHT][Conor::MAP_WIDTH];
	int roundCount;
	float nextRoundTimer;
	int min;
	float sec = 0;
	//network variables
	Net net;
	char remoteIP[16];
	int port;
	char localIP[16];
	UINT gameType;
	toServerStc toServerData;
	ToClientStc toClientData;
	int playerCount;
	float netTime;
	int error;


	enum MENUSTATE{
		START_OPTION,
		OPTIONS_OPTION,
		EXIT_OPTION,
		BACK_OPTION,
	};

	MENUSTATE state;
	
public:
	
    // Constructor
    Bomberman();
    // Destructor
	virtual ~Bomberman();
    // Initialize the game
    void initialize(HWND hwnd);
    void update();      // must override pure virtual from Game
    void ai();          // "
    void collisions();  // "
    void render();      // "
    void releaseAll();
    void resetAll();
	void roundStart();  // start a new round of play
	void handleInput();
	void handleTileCollision(VECTOR2 collisionVector);
	void handleBombs(Player *player);
	void loadLevel();
	bool checkPosition(Player *player,Player *p2);
	void ResetLevel();
	void pointerChange();
	void handleTimer(float frameTime);
	void handleMenuControls(float frameTime);

	//network functions
	void communicate(float frameTime);
	int  initializeServer(int port);
	void checkNetworkTimeout();
	void prepareDataForClient();
	void doClientCommunication();
	void sendInfoToServer();
	void getInfoFromServer();
	void clientWantsToJoin();
	void connectToServer();
};

#endif
