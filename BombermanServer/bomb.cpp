//Garreth Byrne

#include "bomb.h"
#include "audio.h"

//=============================================================================
// default constructor
//=============================================================================
Bomb::Bomb() : Entity()
{
	spriteData.width = ConorBomb::WIDTH;        // size of Player
	spriteData.height = ConorBomb::HEIGHT;
	spriteData.x = ConorBomb::X;                   // location on screen
	spriteData.y = ConorBomb::Y;
	spriteData.rect.bottom = ConorBomb::HEIGHT;    // rectangle to select parts of an image
	spriteData.rect.right = ConorBomb::WIDTH;
	velocity.x = 0;                             // velocity X
	velocity.y = 0;                             // velocity Y
	frameDelay = ConorBomb::BOMB_ANIMATION_DELAY;
	startFrame = ConorBomb::BOMB_END_FRAME;
	endFrame = ConorBomb::BOMB_START_FRAME;
	currentFrame = startFrame;
	radius = 32;
	mass = ConorBomb::MASS;
	collisionType = entityNS::BOX;	
	active = false;
	visible = false;
	isExploded = false;
	fuse = -3.0f;
}

//=============================================================================
// Initialize the bomb.
// Post: returns true if successful, false if failed
//=============================================================================
bool Bomb::initialize(Game *gamePtr, int width, int height, int ncols,
	TextureManager *textureM)
{
	return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}

//=============================================================================
// draw the ship
//=============================================================================
void Bomb::draw()
{
	Image::draw();
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void Bomb::update(float frameTime)
{	
		fuse-= frameTime;
	if (visible = false){
		return;
	}
	if (fuse > 0){
		isExploded = false;
		visible = true;
		active = true;
		
	}
	else{
		visible = false;
		active = false;
		isExploded = true;
	}
	

	Entity::update(frameTime);
}
//=============================================================================
// Name: Plant
// Description: This function places a bomb in front of the players position by checking the players direction
// Parameters: Pointer to the player object and the fuse 
// Returns: none
// By Garreth
//=============================================================================
void Bomb::plant(Entity *player,float p_fuse)
{


	if (fuse <= 0.0f)
	{
		fuse = p_fuse;
		//currentFrame = ConorBomb::BOMB_START_FRAME;
		DIRECTION dir = player->getDirection();
		switch (dir){
		case NORTH:
			spriteData.x = player->getCenterX() - spriteData.width / 2;
			spriteData.y = (player->getCenterY() - spriteData.height / 2) - 32;
			
			visible = true;
			active = true;
		
			break;
		case SOUTH:
			spriteData.x = player->getCenterX() - spriteData.width / 2;
			spriteData.y = (player->getCenterY() - spriteData.height / 2) + 32;
			
			visible = true;
			active = true;
			
			break;
		case EAST:
			spriteData.x = (player->getCenterX() - spriteData.width / 2) - 32;
			spriteData.y = player->getCenterY() - spriteData.height / 2;
			
			visible = true;
			active = true;
			
			break;
		case WEST:
			spriteData.x = (player->getCenterX() - spriteData.width / 2) + 32;
			spriteData.y = player->getCenterY() - spriteData.height / 2;
			
			visible = true;
			active = true;
			
			break;
		}
	}
}
//=================================================
//networking functions
//================================================

BombStc Bomb::getNetData(){
	BombStc data;
	data.X = getX();
	data.Y = getY();
	data.active = active;
	data.isExploded = isExploded;
	return data;
}

