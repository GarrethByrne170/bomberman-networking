#ifndef EXPLOSION_H
#define EXPLOSION_H
#include "entity.h"
#include "constants.h"
#include "player.h"


namespace garrethExplosion
{
	const int WIDTH = 32;                   // image width
	const int HEIGHT = 32;                  // image height
	const int MID_WIDTH = 48;                   // image width
	const int MID_HEIGHT = 48;                  // image height
	const int LARGE_WIDTH = 64;                   // image width
	const int LARGE_HEIGHT = 64;                  // image height
	const int X = 0;   // location on screen
	const int Y = 0;
	const int   COLLISION_RADIUS = WIDTH / 2 + 2;   // for circular collision
	const int   MID_COLLISION_RADIUS = MID_WIDTH / 2 + 2;
	const int   LARGE_COLLISION_RADIUS = LARGE_WIDTH / 2 + 2;
	//const int   TEXTURE_COLS = 1;// 8;
	const int   EXPLOSION_START_FRAME = 1;// 32; // explosion start frame
	const int   EXPLOSION_END_FRAME = 1;// 39;   // explosion end frame
	const float EXPLOSION_ANIMATION_DELAY = 0.1f;   // time between frames
	
}
//networking
struct ExplosionStc
{
	float X, Y;
	bool active;
	float timer;
	bool isExplosionOn;
};
class Explosion : public Entity            // inherits from Entity class
{
private:
	bool explosionOn;
	float timer;
public:
	// constructor
	Explosion();
	void  disable() { visible = false; active = false; }
	void  enable()  { visible = true; active = true; }
	void explode(Entity *bomb);
	void update(float frameTime);
	bool getExplosionOn(){ return explosionOn; }
	void setExplosionOn(bool p){ explosionOn = p; }
	//==========================================
	//Networking
	//==========================================
	ExplosionStc getNetData();
	void setNetDate(ExplosionStc es);
};
#endif