// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 ship.cpp v1.0

#include "player.h"
#include "bomb.h"
#include "game.h"
#include "Bomberman.h"
//=============================================================================
// default constructor
//=============================================================================
Player::Player() : Entity()
{
	spriteData.width = Conor::WIDTH;           // size of Player
	spriteData.height = Conor::HEIGHT;
	spriteData.x = Conor::X;                   // location on screen
	spriteData.y = Conor::Y;
	spriteData.rect.bottom = Conor::HEIGHT;    // rectangle to select parts of an image
	spriteData.rect.right = Conor::WIDTH;
	velocity.x = 0;                             // velocity X
	velocity.y = 0;                             // velocity Y
	frameDelay = Conor::SPRITE_ANIMATION_DELAY;
	startFrame = Conor::IDLE_FRAME;  
	endFrame = Conor::IDLE_FRAME;    
	currentFrame = startFrame;
	radius = Conor::WIDTH / 2.0;
	mass = Conor::MASS;
	collisionType = entityNS::BOX;
	//collisionType = entityNS::CIRCLE;
	setLoop(false);
	walkSpeed = 0.05f;
	score = 0;
	bombPower = 1;
	bombTimer = 3;
	dir = WEST;
	isDead = false;
	//networking
	strcpy_s(netIP, "000.000.000.000");  // IP address as dotted quad; nnn.nnn.nnn.nnn
	timeout = 0;
	connected = false;
	buttons = 0;
	playerN = 0;
	commWarnings = 0;
	commErrors = 0;
}

//=============================================================================
// Initialize the Ship.
// Post: returns true if successful, false if failed
//=============================================================================
bool Player::initialize(Game *gamePtr, int width, int height, int ncols,
	TextureManager *textureM)
{
	//idle.initialize()
	return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}

//=============================================================================
// draw the ship
//=============================================================================
void Player::draw()
{
	Image::draw();              // draw Player

}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void Player::update(float frameTime)
{
	if (active){
		handleInput(frameTime);
	}
	Entity::update(frameTime);
	
}
//=============================================================================
// Name: handleInput
// Description: This function handles the player input from the keyboard and xbox controller
// Parameters: Frame time
// Returns: none
//by Conor
//=============================================================================
void Player::handleInput(float frameTime){
	SHORT movementX = input->getGamepadThumbLX(gamepadNumber);
	SHORT movementY = input->getGamepadThumbLY(gamepadNumber);

	oldX = spriteData.x;                        // save current position
	oldY = spriteData.y;
	if (input->isKeyDown(up) || input->getGamepadDPadUp(gamepadNumber) || movementY > 0)
	{
		dir = NORTH;
		state = WALKING;
		spriteData.y += walkStep * -walkSpeed;
		//spriteData.y += walkStep * velocity.y;
		audio->playCue(WALK);
		startFrame = Conor::UP_START_FRAME;
		endFrame = Conor::UP_END_FRAME;
		setLoop(true);
	}
	else if (input->isKeyDown(down) || input->getGamepadDPadDown(gamepadNumber) || movementY < 0)
	{
		dir = SOUTH;
		state = WALKING;
		spriteData.y -= walkStep * -walkSpeed;
		//spriteData.y -= walkStep * velocity.y;
		audio->playCue(WALK);
		startFrame = Conor::DOWN_START_FRAME;
		endFrame = Conor::DOWN_END_FRAME;
		setLoop(true);

	}
	else if (input->isKeyDown(left) || input->getGamepadDPadRight(gamepadNumber) || movementX > 0)
	{
		dir = WEST;
		state = WALKING;
		spriteData.x += walkStep * walkSpeed;
	//	spriteData.x += walkStep * velocity.x;
		audio->playCue(WALK);
		startFrame = Conor::LEFT_START_FRAME;
		endFrame = Conor::LEFT_END_FRAME;
		setLoop(true);

	}
	else if (input->isKeyDown(right) || input->getGamepadDPadLeft(gamepadNumber) || movementX < 0)
	{
		dir = EAST;
		state = WALKING;
		spriteData.x -= walkStep * walkSpeed;
		//spriteData.x -= walkStep * velocity.x;
		audio->playCue(WALK);
		startFrame = Conor::RIGHT_START_FRAME;
		endFrame = Conor::RIGHT_END_FRAME;
		setLoop(true);

	}

	else if (input->getGamepadBack(gamepadNumber))
	{
		PostQuitMessage(0);
	}
	else
	{
		startFrame = Conor::IDLE_FRAME;
		endFrame = Conor::IDLE_FRAME;
		state = STANDING;
	}
}

//=============================================================================
// Name: addPowerUp
// Description: Adds stats to the player based on the Power ups ID
// Parameters: Reference to the power up
// Returns: none
// Garreth
//=============================================================================
void Player::addPowerUP(PowerUp &p){
	int temp = p.getID();
	switch (temp){
		//speed power up
	case 1:
		if (walkSpeed < 0.15){
			walkSpeed += 0.05;
		}
		break;
	case 2:
		//minus speed power up
		if (walkSpeed > 0.05f){
			walkSpeed -= 0.05;
		}
		break;
		//increase power
	case 3:
		if (bombPower < 3){
			bombPower++;
		}
		break;
		//decrease power
	case 4:
		if (bombPower > 1){
			bombPower--;
		}
		break;
	
		//decrease fuse time
	case 5:
		if (bombTimer > 1){
			//take 1 sec off the timer
			bombTimer -= 1;
		}
		break;
		//increase fuse time
	case 6:
		if (bombTimer < 3){
			//add 1 sec to the timer
			bombTimer += 1;
		}
		break;
		//add armour to survive one hit		
	case 7:
	//increase score by 5
		score += 5;
		break;
	}
}
//=============================================================================
// Name: resetStats
// Description: Resets the stats of the player and will be called get round
// Parameters: none
// Returns: none
// Garreth
//=============================================================================
void Player::resetStats(){
	currentFrame = startFrame;
	walkSpeed = 0.05f;
	bombPower = 1;
	bombTimer = 3;
	dir = WEST;
	isDead = false;
	active = true;
	visible = true;
}

//networking functions
void Player::setNetData(PlayerStc ss)
{
	setX(ss.X);
	setY(ss.Y);
	
	setWalkSpeed(ss.walkSpeed);
	setActive((ss.flags & 0x01) == 0x01);
	setIsDead((ss.flags & 0x02) == 0x02);
	if (active){
		visible = true;
	}
}

//=============================================================================
// Return current player state in PlayerStc
//=============================================================================
PlayerStc Player::getNetData()
{
	PlayerStc data;
	data.X = getX();
	data.Y = getY();
	data.walkSpeed = getWalkSpeed();
	data.score = getScore();
	data.playerN = getPlayerN();
	//-----flags-----
	// bit0 active
	// bit1 isDead
	// bit2 
	data.flags = 0;
	if (getActive())
		data.flags |= 0x01;
	if (getIsDead())
		data.flags |= 0x02;
	
	return data;
}